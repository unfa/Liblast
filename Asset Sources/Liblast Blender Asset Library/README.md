# Liblast Blender Asset Library

## What is this?

This folder contains `.blend` (also `.txt`) file(s) that hold(s) common assets for mapping (creating Liblast levels) in Blender.
For example it has a set representing all generic environment materials that are used in Liblast. In the future also possibly common props and other stuff.

Using materials from this library will mean that after exporting your level from Blender to Godot Engine (via a GLTF file), Godot will be able to identify the materials (by a game-wide unique name) and replace the imported ones with the ones used in the game, that have configured shaders etc.

The materials in the Library are purely for providing an accurate visual representation of the in-game materials, to make level design easier. The could be placeholders, but this would make working on environemnt visuals really daunting.

The materials utilize the exact same textures as the ones used in the game. This means that any update to game's environment textures will be immediately reflected in the Liblast Blender Asset Library - meaning your levels will stay looking consistent with the current state of the game.

## How to use this Library?

To use this Library, you'll need to add it to your Asset Libraries in Blender.
To do so, open the Preferences (`Edit > Preferences` from the main menu) and go to the `File Paths` tab at the bottom of the list.
Here you'll find the `Asset Libraries` panel.

Click the `+` button and navigate or copy/paste the `Liblast/Asset Sources/Liblast Blender Asset Library/` folder path (you'll need to find the ful absolute path to that folder in your system - that depends on what OS you use and where did you put the Liblast git repository).

Once you confirm, the Liblast Blender Asset Library should be added to your list.
Select it and make sure the `Import Method` is set to `Link`. We don't want to copy these assets around!
By having the assets referenced (linked) from the original files, you'll have any changes to the Library be reflected in your Blender projects, without requiring additional work.

Once your Asset Libraries are configured, don't forget to **save the settings!**

To do that, open the "hamburger menu" (a button with thre parrarel horizontal lines on it), and select `Save Preferences`.

## I have the Library, what now?

Now the Liblast Asset Library will appear in the `Asset Browser` view in Blender, whenever you open it. You'll be able to simply drag and drop materials from there onto your map's geometry.

## A warning about relative paths

If your Blender projects use relative external data file paths (and they should be, in the context of Liblast!), moving your `.blend` file will break these paths and you'll need to use the `File > External Data > Find Missing Files...` option to fix the file references. To prevent that from happening, use `File > Save As...` from within Blender instead of moving your file with a file manager. Blender will update the patsh automatically.

As long as your project files are properly located inside the Liblast git repository, these relative paths will allow the projects to open without issues for anyone else working on the game, regardless of where they put the Liblast git repository on their system. Absolute file paths would mean every single person would need to do the `Find Missing Files...` procedure every single time. And also inevitabl we'd see a lot of commits containing just the changes to these absolute paths, adding chaos. **That's why we stick to relative paths!**

However - don't sewat it! If the textures are missing, that's only going to result in a broken preview in Blender. The exported level will be fine, as long as you *keep the material names intact*.

## What is in the Library?

### Materials

There's 3 categories of materials. Usually each material has 3 versions one for each category.
All materials within a category strictly follow the same layouts and edge bevel shape/width which makes them interchangable, allowing reuse of models with different materials.

#### Tileable
Regular seamlessly repeating textures.
Best used to texture large plain surfaces.
No special tools needed.

#### Trimsheet
Horizontally seamless textures.
Best used to texture oblong detail and to add variation to edges, extruded detail, like corridors, edges of large walls etc, break up large plain surfaces etc.
Recommended tools: DreamUV and UltimateTrim Blender add-ons.
The Trimsheet materials come with an atlas model, that can be use with DreamUV to identify the layout and generate correct UVs.


#### Hotspot
Special atlases designed to be used with the DreamUV Blender add-on.
Hotspot atlases allow highly automated texturing of complex geometry, that if used well can rival custom baked textures, but is incredibly flexible and reusable.
The Hotspot materials come with an atlas model, that can be use with DreamUV to identify the layout and generate correct UVs.


> Good luck!
>
> -- unfa 2023-08-17
