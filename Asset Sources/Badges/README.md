# How to add Badges

This requires Inkscape 1.2 or newer.
The Badges asset workflow depends on Inkscape's batch export function.

To add a new badge - create your design basing on existing ideas:
- must be in the specified circle
- must use the specified FreeSerif font
- must be contained in one Group
- must be exported to a 64x64 PNG by Batch Export > Selection

You need to specify object Label (not Title!) for the badge group.
Then you can selet the badge (or badges) and export Batch > Selection.
Export to Game/Assets/Badges where the new files will be named `Badge_LABEL.png`, where `LABEL` is whatever you specify in object properties for the badge group.

Current export DPI is 25.6 which makes the existing badges produce a 64x64 PNG file.

In Godot make sure the texture is stored uncompressed.

- unfa 2022-06-21
