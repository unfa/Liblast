# Texturing

After the lowpoly and highpoly models are done, and all necessary maps are baked, we can start processing them in Material Maker to arrive at our final PBR material for the model.

Maps that we need to bake from Blender are:

- Normals (tangent-space) for surface details
- Material ID for compositing different submaterials together in MM
- Ambient Occlusion - for further surface details
- Object-Space Normals - for triplanar mapping within MM
- UV Layout - for bevel correction in MM

In Material Maker we also bake out:
- Inverse_UV (Object-Space position) to be able to do triplanar mapping - ideally we'd bake this out from HP model onto LP model from Blender, but it's not big deal

The lowpoly model needs to be exported into .OBJ for Material Maker's 3D preview (version 1.2 doesn't support GLTF yet)

Inpaint Mask.svg is made in Inkscape on a Normal Map or Material ID map reference to mark ouot parts that will be inpainted using the Dilate Node in MM. This is used to procedurally remove artifacts that happened in baking.

In practice my work was involving a lot of iterating between modelling, baking maps, updating them in Material Maker, drawing the Inpaint Mask etc.

-- unfa 2023-02-11
