These are exported 4K textures from the Material Maker project.
They've been converted from PNG to high-quality WEBP to save on storage space

The have been downscaled to 2K for use in the game.

Conversion from PNG and downscaling was done with ImageMagic convert command using a Bash one-liner:

```
#!/bin/bash
# convert 4K textures to WebP for optimized storage
for i in *.png; do convert $i -quality 100 $i.webp; done
# downscale and covert to WebP for importing in Godot
for i in *.png; do convert $i -resize 50% -quality 100 ../../../../../Game/Assets/Weapons/WeaponTypes/ProjectileShootingWeapon/PlasmaGun/$i.webp; done
```

**Don't commit the PNG files, only commit WebP!**

-- unfa 2023-02-11
