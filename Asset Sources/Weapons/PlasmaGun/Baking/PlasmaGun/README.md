# Baking

Here's maps baked from Blender using Bysted's Blender Baker.

They are all 16-bit (per-channel) RGB PNGs except for the AO map which is 16-bit grayscale (I had to manually convert it to grayscale using GIMP).

To get 16-bit maps, and not 8-bit, make sure you read [this](https://codeberg.org/unfa/BystedsBlenderBakerPlus). 16-bits are necesry for normal maps specifically. At least for processing in Material Maker.

Before comiting to the repository make sure that all image files are as small as possible without affectign the quality of the result:

1. Make sure AO maps are denoised - noise is extremely hard to compress, and when not needed it's a waste of space. If we'll be denoising an AO map anyway - do it before commiting to the git repo. Bysted's Blender Baker will denoise AO maps automatically. but it won't make sure the fiels are Grayscale (and will save it as RGB, wasting lots of space). So...
2. Make sure whatever doesn't need color is a Grayscale file (1 color channel) - this reduces file size to 33% - I did this manually wiht GIMP for this AO map
3. Proces all png files with `optipng` to improve the lossless compression ratio. You can simply run `optipng *.png` in the directory with image files and it'll iterate and overwrite fiels where size was reduced (with no quality loss!).
4. Where possible I'd also recommend using WebP instead of PNG, as it has an even beter compression ratio, even with lossless quality

-- unfa 2023-02-11
