# Character asset workflow

## Texturing

The character model is UV-unwrapped (and modelled) with symmetry (the left and right sides of the character use the same texture space).

The character model is using Blender's Dynamic Paint feature to mark parts of the model that will be then interpreted by the in-game shader.

To update the character SDF texture you need to:

1.  Modify the set of "brush" meshes that intersect the T-posed character model. Make sure the meshes are set as Dynamic Paint Brushes and use pure white color. In the future we might separate RGB channels and use them separately for different things, right now it's only grayscale though.

2. Select the T-posed characted model, go to the physics tab, Dynamic Paint and click "Bake Image Sequence". This will export a 4K (maximum resolution) texture into a pre-determined location (`./Textures/ID_Mask0001.png`). You can reload that texture used in the preview material to see the effect in the 3D viewport.

3. Once you're happy with the result it's time to process the baked image using Material Maker. Open the program and load the `./Textures/ID_Mask_Process.ptex` project file. Material Maker should load your Blender-exported image and process it to produce an SDF texture.

4. Open the 2D Preview in Material Maker, select the Buffer node withing the "OUTPUT" frame and wait until a 1K grayscale SDF texture is generated.

5. Time to export. On subsequent exports you can just use the `File > Export again (Ctrl + E)` option. It should ask you to confirm overwriting existing files (a `.tres` Godot material file). If this dialog doesn't appear or you're exporting first time after pulling, it means, we need to manually point MM into the right export location. To do this, open `File > Export > Godot 4 (Standard)` menu option and navigate to `../../../../Game/Assets/Characters/`. Then set the output filename as `CharacterHeavy.tres` and hit `Export`. Because the Material Output node has nothing connected, the only image it'll write to disk will be the one from the `Export` node. The `CharacterHeavy.tres` file is listed in local `.gitignore` - it won't clutter the git staging area. I've kept it around because when exporting you can just select it to use it's filename and set correct export path nad file name. It is not used by the game however.

6. Once you've exported the 1K SDF texture, you can open the `Character.tscn` scene in Liblast's Godot project and see the effect as the texture is interpreted by the custom shader.

7. Before you commit your changes to textures, please use `optipng` program to reduce the size of the texture images. It's not much, but it adds up.
	On Linux: navigate to the folders containing textures (both in `Asset Sources/Characters` and in `Game/Assets/Characters/` and just run `optipng *.png` to automatically reduce the size of texture files. Thanks :)

-- unfa
