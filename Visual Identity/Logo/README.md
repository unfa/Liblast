# Liblast Logos

Good practices:

- Please use Inkskape’s Batch Export feature to generate the Exports
- Set text as stroke for exported SVG, but keep the text as text somewhere in the Logo Assets.svg source
- Remove all images in `/Logo Exports` before a new export

## Export params

Make 2 export lines in Inkscape Export > Batch Export > Pages

- `No suffix |       PNG | 200 DPI`
- `No suffix | PLAIN SVG | -------`
- Path should be `…/Liblast/Visual Identity/Logo/Logo Exports/liblast` (the last /liblast is important and will avoid a [bug](https://gitlab.com/inkscape/inkscape/-/issues/3533) in Inkscape)