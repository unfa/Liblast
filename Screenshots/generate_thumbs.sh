#!/bin/bash
for i in *.png; do
	convert $i -resize 20% -quality 100 $(basename $i .png)_thumb.jpg
done
