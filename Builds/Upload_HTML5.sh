#!/bin/bash
echo -e "Uploading the HTML5 export to play.libla.st...\n\n\n"
cd ./HTML5/
rsync -av --progress ./* unfa.xyz:~/Liblast_HTML5 && echo -e "\n\n\nUPLOAD COMPLETE!" || echo -e "\nUPLOAD FAILED.\nDo you have necessary credentials?"
cd ..
