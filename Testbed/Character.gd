extends Node3D

@onready var input = $PlayerInput

func set_player(pid: int):
	name = str(pid)
	$Label3D.text = str(pid)
	$PlayerInput.set_player(pid)


func _process(delta):
	if multiplayer.has_multiplayer_peer() and is_multiplayer_authority():
		# Apply the user input
		input.apply(self, delta)


func _on_player_input_ward_placed(ward_type):
	print("Player %s is trying to place a ward." % name)
