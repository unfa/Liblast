extends MultiplayerSpawner

func _spawn_custom(data):
	if typeof(data) != TYPE_INT:
		push_error("Invalid player spawn received")
		return
	var pid : int = data
	var character = preload("res://Character.tscn").instantiate()
	character.set_player(pid)
	return character
