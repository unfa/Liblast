class_name DamageIgnite extends DamageHit

# This class describes damage being done from directly being lit on fire
# For example, when a flamethrower or incendiary device hits the player

func kill_message():
	return "Player died from being engulfed in flames by TODO"
