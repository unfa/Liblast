extends Resource
class_name CharLoadout

# Holds information about weapons a character is equipped with

@export var primary := Weapons.Weapon.NONE
@export var secondary := Weapons.Weapon.NONE
@export var tertiary := Weapons.Weapon.NONE
