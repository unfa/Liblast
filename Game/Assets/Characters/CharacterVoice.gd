extends Resource
class_name CharVoice

@export var jump: AudioStream
@export var land: AudioStream
@export var hurt: AudioStream
@export var die: AudioStream

@export var spawn: Array[AudioStream]
@export var kill: Array[AudioStream]
