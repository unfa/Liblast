extends Resource
class_name ExtendedMaterialDamageEffect

@export var particle_effect: PackedScene
@export var sound_effect: PackedScene
@export var decal: PackedScene
