extends Resource
class_name ExtendedMaterialType

@export var damage_effect: Resource
@export var footstep_sound: AudioStream # TODO: extend this with a custom resource holding a collection of sounds
