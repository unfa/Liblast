extends "res://Assets/Menu/Menu.gd"

func on_mouse_sensitivity_set(sensitivity):
	if GUI:
		GUI.set_mouse_sensitivity(sensitivity)
		$MouseSensitivity.save_data()
