extends "res://Assets/Menu/MenuData.gd"

func on_label_changed():
	self.text = label

func on_value_changed():
	if value:
		self.button_pressed = value

# Seperate sound from on_toggle to prevent playing sound
# on manual setting with on_label_change
func on_pressed():
	$ClickSound.play()

func on_toggled(button_pressed):
	set_var(button_pressed)

func _on_MenuCheckButton_mouse_entered():
	$HoverSound.play()
