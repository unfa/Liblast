# This class is not used fro storage - only for reference

#class_name PlayerAccountRecord

# auth
var username : String
var email : String
var password_hash : String
var password_salt : String
var tokens : Array[String]

var display_name : String
var face_texture : Texture2D
var body_color : Color

var badges : Array [int] # badge enums go here, the InfraServer doesn't need to know what they mean
var achievements : Array
var friend_accounts : Array
var blocked_accounts : Array
var account_kicks : Array
var account_bans : Array

func _init(username: String, password_hash: String, password_salt: String) -> void:
	self.username = username
	self.password_hash = password_hash
	self.password_salt = password_salt
