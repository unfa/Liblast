extends Node

enum Table {PLAYER_ACCOUNTS, TOKENS, GAME_SERVERS}

#enum StoreMode {
#	CREATE, # adding new records
#	APPEND, # appending data to existing records
#	UPDATE, # overwrite existing records
#	DELETE, # delete records
#	}

enum DatabaseBackend {TESTING, PRODUCTION}
var backend = DatabaseBackend.TESTING

var testing_database = {
	Table.PLAYER_ACCOUNTS : {},
	Table.TOKENS : [],
	Table.GAME_SERVERS : {},
	}

var testing_filename = "user://testing_database.txt"

func verify_token(username_hash: String, token: Array):
	if backend == DatabaseBackend.TESTING:
		if testing_database[Table.TOKENS].has(token):
			print("Token found")
			if testing_database[Table.PLAYER_ACCOUNTS][username_hash]["tokens"].has(token):
				return true
			else:
				print("Token is valid but not for provided username_hash")
				return false
		else:
			print("Token not found")
			return false

func save_testing_file() -> int:
	print("Saving testing_database file")
	var testing_file = FileAccess.open(testing_filename,FileAccess.WRITE)
	var err
	if testing_file:
		testing_file.store_string(var_to_str(testing_database))
		err = testing_file.get_error()
		if err == OK:
			print("Success writing testing_database")
			testing_file.flush()
			return OK
		else:
			prints("Failure writing testing_database. Error:", error_string(err))
			return err
	else:
		prints("Can't open testing_database file for writing; error:", error_string(err))
		return err

func retrieve(table, key):
	prints("Attempting to retrieve record", key,"from table",table)
	if backend == DatabaseBackend.TESTING:
		if testing_database[table].has(key):
			print("Retrived record from the testing_database:", var_to_str(testing_database[table][key]))
			return testing_database[table][key]
		else:
			print("Record not found.")
			return null

func store(table, key, record): #, store_mode: StoreMode = StoreMode.CREATE):
	if backend == DatabaseBackend.TESTING:
		if testing_database[table].has(key):
			prints("Attempting to store an already present database record. Aborting.")
			return ERR_ALREADY_EXISTS
		else:
			testing_database[table][key] = record # store the record
			return save_testing_file()

func store_user_token(username_hash: String, token: Array):
	if backend == DatabaseBackend.TESTING:
		if testing_database[Table.PLAYER_ACCOUNTS].has(username_hash):
			prints("Adding a token to existing user account", username_hash)
			testing_database[Table.PLAYER_ACCOUNTS][username_hash]["tokens"].append(token)
			testing_database[Table.TOKENS].append(token)
			# TODO - distribute the new token to all game servers
			return save_testing_file()
		else:
			prints("Attempted adding a token for a missing account", username_hash)
			return ERR_DOES_NOT_EXIST

func retrieve_user_record_item(username_hash: String, key: String):
	if testing_database[Table.PLAYER_ACCOUNTS].has(username_hash):
		if testing_database[Table.PLAYER_ACCOUNTS][username_hash].has(key):
			return testing_database[Table.PLAYER_ACCOUNTS][username_hash][key]

	return null

func modify_user_record_item(username_hash: String, key: String, data):
	if backend == DatabaseBackend.TESTING:
		if testing_database[Table.PLAYER_ACCOUNTS].has(username_hash):
			testing_database[Table.PLAYER_ACCOUNTS][username_hash][key] = data
			prints("Modified user record. New value:", testing_database[Table.PLAYER_ACCOUNTS][username_hash][key])
			return save_testing_file()
		else:
			print("Attempting to modify record for a non-existing user!")
			return ERR_DOES_NOT_EXIST

func _ready():
	if backend == DatabaseBackend.TESTING:
		# load or create database tile
		var testing_file = FileAccess.open(testing_filename, FileAccess.READ)
		if testing_file:
			print("Testing database file exists!")

			print("Loading testing database...")
			testing_database = str_to_var(testing_file.get_as_text())
			if testing_file.get_error() == OK:
				print("Done.")
			else:
				print("Failed. Error: ", testing_file.get_error())
		else:
			var result = save_testing_file()
			assert(result == OK, "Can't save testing_database file!")
